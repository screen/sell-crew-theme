<?php $page = get_page_by_title( 'landing' ); ?>
<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-end justify-content-center">
                    <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                    <div class="footer-item col-xl-3 col-lg-4 col-md-3 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="200">
                        <ul id="sidebar-footer1" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                    <div class="footer-item col-xl-3 col-lg-4 col-md-3 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="250">
                        <ul id="sidebar-footer2" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-3' ) ) : ?>
                    <div class="footer-item col-xl-3 col-lg-4 col-md-3 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="300">
                        <ul id="sidebar-footer3" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-3' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <div class="big-hero-certificate col-xl-3 col-lg-4 col-md-3 col-sm-12 col-12">
                        <h3 data-aos="fade-in" data-aos-delay="350"><?php echo get_post_meta($page->ID, 'scw_home_bighero_logos_title', true); ?></h3>
                        <div class="row align-items-center justify-content-center">
                            <?php $services_list = get_post_meta($page->ID, 'scw_home_bighero_logos_group', true); ?>
                            <?php foreach ($services_list as $item) { ?>
                            <div class="certificate-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="350">
                                <?php $bg_banner = wp_get_attachment_image_src($item['logo_id'], 'full', false); ?>
                                <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $item['logo_id'], '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($item['logo_id'], '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            </div>
                            <?php } ?>
                        </div>

                    </div>
                    <div class="w-100"></div>
                </div>
            </div>
        </div>
        <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-between">
                <div class="footer-copy-item col-xl col-lg col-md col-sm-12 col-12"><h5><?php printf( __('Desarrollado por <a href="%s">SMG | Agencia de Marketing Digital</a>', 'sellcrew'), 'https://www.screenmediagroup.com'); ?></h5></div>
                <div class="footer-copy-item col-xl col-lg col-md col-sm-12 col-12"><h5>Copyright &copy; 2021 - Sell Crew | <a href="<?php echo home_url('/privacy-policy');; ?>"><?php _e('Políticas de Privacidad', 'sellcrew'); ?></a> <a href="https://www.instagram.com/sellcrewexperts/" target="_blank" title="<?php _e('Visita nuestro instagram', 'sellcrew') ?>" class="footer-social"><i class="fa fa-instagram"></i></a></h5></div>
                <div class="footer-copy-item col-xl col-lg col-md col-sm-12 col-12"></div>
                </div>
            </div>
            
        </div>
    </div>
</footer>
<?php if ($page->ID == get_queried_object_id()) { ?>
<!-- MODAL CONTACT -->
<div class="modal modal-contact fade" id="contactModal" tabindex="-1" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'scw_home_modal_title', true)); ?>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php get_template_part('templates/modal-form-template'); ?>
            </div>
            <div id="loaderForm" class="contact-form-response contact-form-response-hidden col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="loader"></div>
            </div>
            <div id="formResult" class="contact-form-result contact-form-result-hidden"></div>
        </div>
    </div>
</div>

<!-- MODAL PLAN -->
<div class="modal modal-plan fade" id="planModal" tabindex="-1" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div id="planInfoModal" class="plan-info-modal col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                        </div>
                        <div class="plan-info-form col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'scw_home_modal_title', true)); ?>
                            <?php get_template_part('templates/modal-plan-template'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="loaderPlan" class="contact-form-response contact-form-response-hidden col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="loader"></div>
            </div>
            <div id="resultPlan" class="contact-form-result contact-form-result-hidden"></div>
        </div>
    </div>
</div>
<?php } ?>

<?php wp_footer() ?>
</body>

</html>