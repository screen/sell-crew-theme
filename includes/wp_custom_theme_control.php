<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action( 'customize_register', 'sellcrew_customize_register' );

function sellcrew_customize_register( $wp_customize ) {

    /* HEADER */
    $wp_customize->add_section('scw_header_settings', array(
        'title'    => __('Header', 'sellcrew'),
        'description' => __('Header elements options', 'sellcrew'),
        'priority' => 30
    ));

    $wp_customize->add_setting('scw_header_settings[button_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'button_text', array(
        'type' => 'text',
        'label'    => __('Button Text', 'sellcrew'),
        'description' => __( 'Add the descriptive text for the header button', 'sellcrew' ),
        'section'  => 'scw_header_settings',
        'settings' => 'scw_header_settings[button_text]'
    ));

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('scw_social_settings', array(
        'title'    => __('Redes Sociales', 'sellcrew'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'sellcrew'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('scw_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'sellcrew_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'scw_social_settings',
        'settings' => 'scw_social_settings[facebook]',
        'label' => __( 'Facebook', 'sellcrew' ),
    ));

    $wp_customize->add_setting('scw_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'sellcrew_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'scw_social_settings',
        'settings' => 'scw_social_settings[twitter]',
        'label' => __( 'Twitter', 'sellcrew' ),
    ));

    $wp_customize->add_setting('scw_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'sellcrew_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'scw_social_settings',
        'settings' => 'scw_social_settings[instagram]',
        'label' => __( 'Instagram', 'sellcrew' ),
    ));

    $wp_customize->add_setting('scw_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'sellcrew_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'scw_social_settings',
        'settings' => 'scw_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'sellcrew' ),
    ));

    $wp_customize->add_setting('scw_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'sellcrew_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'youtube', array(
        'type' => 'url',
        'section' => 'scw_social_settings',
        'settings' => 'scw_social_settings[youtube]',
        'label' => __( 'YouTube', 'sellcrew' ),
    ) );

    /* COOKIES SETTINGS */
    $wp_customize->add_section('scw_cookie_settings', array(
        'title'    => __('Cookies', 'sellcrew'),
        'description' => __('Opciones de Cookies', 'sellcrew'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('scw_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'sellcrew'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'scw_cookie_settings',
        'settings' => 'scw_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('scw_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'scw_cookie_settings',
        'settings' => 'scw_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'sellcrew' ),
    ) );

}

function sellcrew_sanitize_url( $url ) {
    return esc_url_raw( $url );
}

/* --------------------------------------------------------------
CUSTOM CONTROL PANEL
-------------------------------------------------------------- */
/*
function register_sellcrew_settings() {
    register_setting( 'sellcrew-settings-group', 'monday_start' );
    register_setting( 'sellcrew-settings-group', 'monday_end' );
    register_setting( 'sellcrew-settings-group', 'monday_all' );
}

add_action('admin_menu', 'sellcrew_custom_panel_control');

function sellcrew_custom_panel_control() {
    add_menu_page(
        __( 'Panel de Control', 'sellcrew' ),
        __( 'Panel de Control','sellcrew' ),
        'manage_options',
        'sellcrew-control-panel',
        'sellcrew_control_panel_callback',
        'dashicons-admin-generic',
        120
    );
    add_action( 'admin_init', 'register_sellcrew_settings' );
}

function sellcrew_control_panel_callback() {
    ob_start();
?>
<div class="sellcrew-admin-header-container">
    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="sellcrew" />
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
</div>
<form method="post" action="options.php" class="sellcrew-admin-content-container">
    <?php settings_fields( 'sellcrew-settings-group' ); ?>
    <?php do_settings_sections( 'sellcrew-settings-group' ); ?>
    <div class="sellcrew-admin-content-item">
        <table class="form-table">
            <tr valign="center">
                <th scope="row"><?php _e('Monday', 'sellcrew'); ?></th>
                <td>
                    <label for="monday_start">Starting Hour: <input type="time" name="monday_start" value="<?php echo esc_attr( get_option('monday_start') ); ?>"></label>
                    <label for="monday_end">Ending Hour: <input type="time" name="monday_end" value="<?php echo esc_attr( get_option('monday_end') ); ?>"></label>
                    <label for="monday_all">All Day: <input type="checkbox" name="monday_all" value="1" <?php checked( get_option('monday_all'), 1 ); ?>></label>
                </td>
            </tr>
        </table>
    </div>
    <div class="sellcrew-admin-content-submit">
        <?php submit_button(); ?>
    </div>
</form>
<?php
    $content = ob_get_clean();
    echo $content;
}
*/
