<?php
/* --------------------------------------------------------------
1.- MAIN HERO
-------------------------------------------------------------- */
$cmb_hero_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_hero_metabox',
    'title'         => esc_html__( 'Section: Main Hero', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_hero_metabox->add_field( array(
    'id'         => $prefix . 'home_hero_main_title',
    'name'       => esc_html__( 'Main Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this hero', 'sellcrew' ),
    'type'       => 'text'
) );

$cmb_hero_metabox->add_field( array(
    'id'         => $prefix . 'home_hero_button_text',
    'name'       => esc_html__( 'Button Text', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive text for this Button', 'sellcrew' ),
    'type'       => 'text'
) );

$cmb_hero_metabox->add_field( array(
    'id'         => $prefix . 'home_hero_button_url',
    'name'       => esc_html__( 'Button URL', 'sellcrew' ),
    'desc'       => esc_html__( 'Add the Link URL for this Button', 'sellcrew' ),
    'type'       => 'text_url'
) );

/* --------------------------------------------------------------
2.- PARTNERS
-------------------------------------------------------------- */
$cmb_partners_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_partners_metabox',
    'title'         => esc_html__( 'Section: Partners', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_partners_metabox->add_field( array(
    'id'         => $prefix . 'home_partners_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$group_field_id = $cmb_partners_metabox->add_field( array(
    'id'          => $prefix . 'home_partners_list',
    'type'        => 'group',
    'description' => __( 'Logos', 'sellcrew' ),
    'options'     => array(
        'group_title'       => __( 'Logos {#}', 'sellcrew' ),
        'add_button'        => __( 'Agregar otro Logos', 'sellcrew' ),
        'remove_button'     => __( 'Remover Logos', 'sellcrew' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Logos?', 'sellcrew' )
    )
) );

$cmb_partners_metabox->add_group_field( $group_field_id, array(
    'id'   => 'logo',
    'name'      => esc_html__( 'Logo', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Logo para este Servicio', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );
$cmb_partners_metabox->add_group_field( $group_field_id, array(
    'id'   => 'logo_gray',
    'name'      => esc_html__( 'Logo en Gris Azulado', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Logo para este Servicio', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );

/* --------------------------------------------------------------
3.- SERVICES
-------------------------------------------------------------- */
$cmb_services_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_services_metabox',
    'title'         => esc_html__( 'Section: Services', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_services_metabox->add_field( array(
    'id'         => $prefix . 'home_services_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$group_field_id = $cmb_services_metabox->add_field( array(
    'id'          => $prefix . 'home_services_group',
    'type'        => 'group',
    'description' => __( 'Servicios', 'sellcrew' ),
    'options'     => array(
        'group_title'       => __( 'Servicio {#}', 'sellcrew' ),
        'add_button'        => __( 'Agregar otro Servicio', 'sellcrew' ),
        'remove_button'     => __( 'Remover Servicio', 'sellcrew' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Servicio?', 'sellcrew' )
    )
) );

$cmb_services_metabox->add_group_field( $group_field_id, array(
    'id'   => 'icon',
    'name'      => esc_html__( 'Icono del Servicio', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Icono para este Servicio', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Icono', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );

$cmb_services_metabox->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Servicio', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese un titulo alusivo al Servicio', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_services_metabox->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Servicio', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Servicio', 'sellcrew' ),
    'type' => 'textarea_small'
) );

/* --------------------------------------------------------------
4.- HOW-TO SECTION
-------------------------------------------------------------- */
$cmb_howto_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_howto_metabox',
    'title'         => esc_html__( 'Section: How-to', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_howto_metabox->add_field( array(
    'id'         => $prefix . 'home_howto_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$group_field_id = $cmb_howto_metabox->add_field( array(
    'id'          => $prefix . 'home_howto_group',
    'type'        => 'group',
    'description' => __( 'Pasos', 'sellcrew' ),
    'options'     => array(
        'group_title'       => __( 'Paso {#}', 'sellcrew' ),
        'add_button'        => __( 'Agregar otro Paso', 'sellcrew' ),
        'remove_button'     => __( 'Remover Paso', 'sellcrew' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Paso?', 'sellcrew' )
    )
) );

$cmb_howto_metabox->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Paso', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese un titulo alusivo al Paso', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
5.- PLANS
-------------------------------------------------------------- */
$cmb_plans_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_plans_metabox',
    'title'         => esc_html__( 'Section: Plans', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_plans_metabox->add_field( array(
    'id'         => $prefix . 'home_plans_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$group_field_id = $cmb_plans_metabox->add_field( array(
    'id'          => $prefix . 'home_plans_group',
    'type'        => 'group',
    'description' => __( 'Planes', 'sellcrew' ),
    'options'     => array(
        'group_title'       => __( 'Plan {#}', 'sellcrew' ),
        'add_button'        => __( 'Agregar otro Plan', 'sellcrew' ),
        'remove_button'     => __( 'Remover Plan', 'sellcrew' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Plan?', 'sellcrew' )
    )
) );

$cmb_plans_metabox->add_group_field( $group_field_id, array(
    'id'   => 'icon',
    'name'      => esc_html__( 'Icono del Plan', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Icono para este Plan', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Icono', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );

$cmb_plans_metabox->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Plan', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese un titulo alusivo al Plan', 'sellcrew' ),
    'type' => 'text'
) );

$cmb_plans_metabox->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Plan', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Plan', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_plans_metabox->add_group_field( $group_field_id, array(
    'id'        => 'button_link',
    'name'      => esc_html__( 'Link URL del Botón', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese una URL para el Botón', 'sellcrew' ),
    'type' => 'text_url'
) );

$cmb_plans_metabox->add_group_field( $group_field_id, array(
    'id'        => 'modal_desc',
    'name'      => esc_html__( 'Descripción del Plan dentro de Modal', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Plan', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
6.- COMPANIES
-------------------------------------------------------------- */
$cmb_companies_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_companies_metabox',
    'title'         => esc_html__( 'Section: Companies', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_companies_metabox->add_field( array(
    'id'         => $prefix . 'home_companies_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$group_field_id = $cmb_companies_metabox->add_field( array(
    'id'          => $prefix . 'home_companies_list',
    'type'        => 'group',
    'description' => __( 'Logos', 'sellcrew' ),
    'options'     => array(
        'group_title'       => __( 'Logos {#}', 'sellcrew' ),
        'add_button'        => __( 'Agregar otro Logos', 'sellcrew' ),
        'remove_button'     => __( 'Remover Logos', 'sellcrew' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Logos?', 'sellcrew' )
    )
) );

$cmb_companies_metabox->add_group_field( $group_field_id, array(
    'id'   => 'logo',
    'name'      => esc_html__( 'Logo', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Logo para este Servicio', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );
$cmb_companies_metabox->add_group_field( $group_field_id, array(
    'id'   => 'logo_gray',
    'name'      => esc_html__( 'Logo en Gris Azulado', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Logo para este Servicio', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );

/* --------------------------------------------------------------
7.- TESTIMONIALS
-------------------------------------------------------------- */
$cmb_testimonials_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_testimonials_metabox',
    'title'         => esc_html__( 'Section: Testimonials', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_testimonials_metabox->add_field( array(
    'id'         => $prefix . 'home_testimonials_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$group_field_id = $cmb_testimonials_metabox->add_field( array(
    'id'          => $prefix . 'home_testimonials_group',
    'type'        => 'group',
    'description' => __( 'Testimonios', 'sellcrew' ),
    'options'     => array(
        'group_title'       => __( 'Testimonio {#}', 'sellcrew' ),
        'add_button'        => __( 'Agregar otro Testimonio', 'sellcrew' ),
        'remove_button'     => __( 'Remover Testimonio', 'sellcrew' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Testimonio?', 'sellcrew' )
    )
) );

$cmb_testimonials_metabox->add_group_field( $group_field_id, array(
    'id'   => 'logo',
    'name'      => esc_html__( 'Logo del Testimonio', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Logo para este Plan', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );

$cmb_testimonials_metabox->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Testimonio', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese un titulo alusivo al Testimonio', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_testimonials_metabox->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Testimonio', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Testimonio', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_testimonials_metabox->add_group_field( $group_field_id, array(
    'id'        => 'author',
    'name'      => esc_html__( 'Autor del Testimonio', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese el Autor del Testimonio', 'sellcrew' ),
    'type'      => 'text'
) );

$cmb_testimonials_metabox->add_group_field( $group_field_id, array(
    'id'        => 'jobtitle',
    'name'      => esc_html__( 'Puesto de trabajo', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese un Puesto de trabajo alusivo al autor', 'sellcrew' ),
    'type'      => 'text'
) );


/* --------------------------------------------------------------
8.- ABOUT
-------------------------------------------------------------- */
$cmb_about_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_about_metabox',
    'title'         => esc_html__( 'Section: About', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_about_metabox->add_field( array(
    'id'         => $prefix . 'home_about_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$cmb_about_metabox->add_field( array(
    'id'         => $prefix . 'home_about_desc',
    'name'       => esc_html__( 'Main Section Description', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_about_metabox->add_field( array(
    'id'         => $prefix . 'home_about_image',
    'name'      => esc_html__( 'Icono del Servicio', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Icono para este Servicio', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Icono', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );

$cmb_about_metabox->add_field( array(
    'id'         => $prefix . 'home_about_desc2',
    'name'       => esc_html__( 'Second Section Description', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
8.- ABOUT
-------------------------------------------------------------- */
$cmb_bighero_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_bighero_metabox',
    'title'         => esc_html__( 'Section: Hero Final', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_bighero_metabox->add_field( array(
    'id'         => $prefix . 'home_bighero_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$cmb_bighero_metabox->add_field( array(
    'id'         => $prefix . 'home_bighero_button_text',
    'name'       => esc_html__( 'Button Text', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive text for this Button', 'sellcrew' ),
    'type'       => 'text'
) );

$cmb_bighero_metabox->add_field( array(
    'id'         => $prefix . 'home_bighero_button_url',
    'name'       => esc_html__( 'Button URL', 'sellcrew' ),
    'desc'       => esc_html__( 'Add the Link URL for this Button', 'sellcrew' ),
    'type'       => 'text_url'
) );

$cmb_bighero_metabox->add_field( array(
    'id'         => $prefix . 'home_bighero_logos_title',
    'name'       => esc_html__( 'Main Section Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this section', 'sellcrew' ),
    'type'       => 'text'
) );

$group_field_id = $cmb_bighero_metabox->add_field( array(
    'id'          => $prefix . 'home_bighero_logos_group',
    'type'        => 'group',
    'description' => __( 'Items', 'sellcrew' ),
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'sellcrew' ),
        'add_button'        => __( 'Agregar otro Item', 'sellcrew' ),
        'remove_button'     => __( 'Remover Item', 'sellcrew' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Item?', 'sellcrew' )
    )
) );

$cmb_bighero_metabox->add_group_field( $group_field_id, array(
    'id'   => 'logo',
    'name'      => esc_html__( 'Logo', 'sellcrew' ),
    'desc'      => esc_html__( 'Cargar un Logo para este item', 'sellcrew' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'sellcrew' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'avatar'
) );

$cmb_bighero_metabox->add_group_field( $group_field_id, array(
    'id'        => 'link',
    'name'      => esc_html__( 'URL del Item', 'sellcrew' ),
    'desc'      => esc_html__( 'Ingrese una URL del Item', 'sellcrew' ),
    'type' => 'text_url'
) );

/* --------------------------------------------------------------
9.- MODAL
-------------------------------------------------------------- */
$cmb_modal_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'home_modal_metabox',
    'title'         => esc_html__( 'Section: Modal', 'sellcrew' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' )
) );

$cmb_modal_metabox->add_field( array(
    'id'         => $prefix . 'home_modal_title',
    'name'       => esc_html__( 'Modal Title', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive title for this modal', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_modal_metabox->add_field( array(
    'id'         => $prefix . 'home_modal_success',
    'name'       => esc_html__( 'Modal Success Message', 'sellcrew' ),
    'desc'       => esc_html__( 'add a descriptive Success Message for this modal', 'sellcrew' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );
