<?php
/*
function sellcrew_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'sellcrew' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'sellcrew' ),
		'menu_name'             => __( 'Clientes', 'sellcrew' ),
		'name_admin_bar'        => __( 'Clientes', 'sellcrew' ),
		'archives'              => __( 'Archivo de Clientes', 'sellcrew' ),
		'attributes'            => __( 'Atributos de Cliente', 'sellcrew' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'sellcrew' ),
		'all_items'             => __( 'Todos los Clientes', 'sellcrew' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'sellcrew' ),
		'add_new'               => __( 'Agregar Nuevo', 'sellcrew' ),
		'new_item'              => __( 'Nuevo Cliente', 'sellcrew' ),
		'edit_item'             => __( 'Editar Cliente', 'sellcrew' ),
		'update_item'           => __( 'Actualizar Cliente', 'sellcrew' ),
		'view_item'             => __( 'Ver Cliente', 'sellcrew' ),
		'view_items'            => __( 'Ver Clientes', 'sellcrew' ),
		'search_items'          => __( 'Buscar Cliente', 'sellcrew' ),
		'not_found'             => __( 'No hay resultados', 'sellcrew' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'sellcrew' ),
		'featured_image'        => __( 'Imagen del Cliente', 'sellcrew' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'sellcrew' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'sellcrew' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'sellcrew' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'sellcrew' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'sellcrew' ),
		'items_list'            => __( 'Listado de Clientes', 'sellcrew' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'sellcrew' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'sellcrew' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'sellcrew' ),
		'description'           => __( 'Portafolio de Clientes', 'sellcrew' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'sellcrew_custom_post_type', 0 );
*/
