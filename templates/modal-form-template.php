<div class="container-fluid">
    <div class="row">
        <form id="mainForm" class="contact-form-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row">
                <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input type="text" id="modal_nombre" name="nombre" class="form-control" placeholder="<?php _e('Nombre (requerido)', 'sellcrew'); ?>" />
                    <small class="danger custom-danger d-none error-nombre"></small>
                </div>
                <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input type="text" id="modal_apellido" name="apellido" class="form-control" placeholder="<?php _e('Apellido (requerido)', 'sellcrew'); ?>" />
                    <small class="danger custom-danger d-none error-apellido"></small>
                </div>
                <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input type="email" id="modal_correo" name="correo" class="form-control" placeholder="<?php _e('Correo Electrónico (requerido)', 'sellcrew'); ?>" />
                    <small class="danger custom-danger d-none error-correo"></small>
                </div>
                <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input type="phone" id="modal_telefono" name="telefono" class="form-control" placeholder="<?php _e('Teléfono', 'sellcrew'); ?>" />
                    <small class="danger custom-danger d-none error-telefono"></small>
                </div>
                <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input type="text" id="modal_empresa" name="empresa" class="form-control" placeholder="<?php _e('Empresa', 'sellcrew'); ?>" />
                    <small class="danger custom-danger d-none error-empresa"></small>
                </div>
                <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input type="text" id="modal_website" name="website" class="form-control" placeholder="<?php _e('Website', 'sellcrew'); ?>" />
                    <small class="danger custom-danger d-none error-website"></small>
                </div>
                <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <textarea name="message" id="modal_message" cols="30" rows="3" class="form-control" placeholder="<?php _e('Mensaje', 'sellcrew'); ?>"></textarea>
                    <small class="danger custom-danger d-none error-message"></small>
                </div>
                <div class="contact-form-submit col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <input id="modalSelected" type="hidden" name="modal_origin" value="modal" />
                    <button id="submitBtn" class="btn btn-lg btn-submit"><?php _e('Enviar', 'sellcrew'); ?></button>
                </div>

            </div>
        </form>
    </div>
</div>