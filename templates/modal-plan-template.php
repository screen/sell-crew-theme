<form id="planForm" class="contact-form-container plan-form-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="row">
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <input type="text" id="plan_nombre" name="nombre" class="form-control" placeholder="<?php _e('Nombre (requerido)', 'sellcrew'); ?>" />
            <small class="danger custom-danger d-none plan-error-nombre"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <input type="text" id="plan_apellido" name="apellido" class="form-control" placeholder="<?php _e('Apellido (requerido)', 'sellcrew'); ?>" />
            <small class="danger custom-danger d-none plan-error-apellido"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <input type="email" id="plan_correo" name="correo" class="form-control" placeholder="<?php _e('Correo Electrónico (requerido)', 'sellcrew'); ?>" />
            <small class="danger custom-danger d-none plan-error-correo"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <input type="phone" id="plan_telefono" name="telefono" class="form-control" placeholder="<?php _e('Teléfono', 'sellcrew'); ?>" />
            <small class="danger custom-danger d-none plan-error-telefono"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <input type="text" id="plan_empresa" name="empresa" class="form-control" placeholder="<?php _e('Empresa', 'sellcrew'); ?>" />
            <small class="danger custom-danger d-none plan-error-empresa"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <input type="text" id="plan_website" name="website" class="form-control" placeholder="<?php _e('Website', 'sellcrew'); ?>" />
            <small class="danger custom-danger d-none plan-error-website"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <textarea name="message" id="plan_message" cols="30" rows="3" class="form-control" placeholder="<?php _e('Mensaje', 'sellcrew'); ?>"></textarea>
            <small class="danger custom-danger d-none plan-error-message"></small>
        </div>
        <div class="contact-form-submit col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <input id="planSelected" type="hidden" name="plan_origin" />
            <button id="submitPlanBtn" class="btn btn-lg btn-submit"><?php _e('Enviar', 'sellcrew'); ?></button>
        </div>
    </div>
</form>