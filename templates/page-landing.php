<?php

/**
 * Template Name: Landing Page
 *
 * @package sellcrew
 * @subpackage sellcrew-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<div class="hero-background">
    <img src="<?php echo get_template_directory_uri(); ?>/images/background-header.png" alt="bubble1" class="parallax-handler">
</div>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="hero-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12">
                        <h1 class="main-title" data-aos="fade-in" data-aos-delay="10"><?php echo get_post_meta(get_the_ID(), 'scw_home_hero_main_title', true); ?></h1>
                        <div class="main-hero-content-text" data-aos="fade-in" data-aos-delay="10">
                            <?php the_content(); ?>
                            <a href="<?php echo get_post_meta(get_the_ID(), 'scw_home_hero_button_url', true); ?>" class="btn btn-md btn-hero btn-special">
                                <span class="btn__ink"></span>
                                <div class="btn__inner">
                                    <?php echo get_post_meta(get_the_ID(), 'scw_home_hero_button_text', true); ?>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="hero-image col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1">
                        <div class="parallax" data-aos="fade-in" data-aos-delay="450">
                            <div class="parallax-layer parallax-layer__1" data-parallax-disallow="y" data-parallax-deep="700">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/main-hero-vector1.png" alt="hero1" class="img-fluid" />
                            </div>
                            <?php $image = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                            <div class="parallax-layer parallax-layer__2" data-parallax-deep="500">
                                <img src="<?php echo $image; ?>" alt="main" class="img-fluid" />
                            </div>
                            <div class="parallax-layer parallax-layer__3" data-parallax-deep="250">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/main-hero-vector2.png" alt="hero2" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="partners-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="partners-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'scw_home_partners_title', true); ?></h2>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center">
                    <?php $gallery_list = get_post_meta(get_the_ID(), 'scw_home_partners_list', true); ?>
                    <?php $gallery_list_gray = get_post_meta(get_the_ID(), 'scw_home_partners_list_gray', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($gallery_list as $item) { ?>
                        <?php $delay = 200 * $i; ?>

                        <div class="partners-item partners-item-<?php echo $i; ?> col" data-aos="fade-in" data-aos-delay="<?php echo $delay; ?>">
                            <div class="partners-item-wrapper">
                                <?php $bg_banner = wp_get_attachment_image_src($item['logo_id'], 'full', false); ?>
                                <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['logo_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['logo_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid img-color" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />

                                <?php $bg_banner = wp_get_attachment_image_src($item['logo_gray_id'], 'full', false); ?>
                                <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['logo_gray_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['logo_gray_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid img-gray" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            </div>

                        </div>

                    <?php $i++;
                        if ($i >= 4) {
                            $i = 1;
                        }
                    } ?>
                </div>
            </div>
        </section>

        <section id="services" class="services-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="service-before">
                <img src="<?php echo get_template_directory_uri(); ?>/images/services-vector1.png" alt="bubble1" class="parallax-handler">
            </div>
            <div class="service-after">
                <img src="<?php echo get_template_directory_uri(); ?>/images/services-vector2.png" alt="bubble2" class="parallax-handler2">
            </div>
            <div class="container">
                <div class="row">
                    <div class="services-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'scw_home_services_title', true); ?></h2>
                    </div>
                    <?php $services_list = get_post_meta(get_the_ID(), 'scw_home_services_group', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($services_list as $item) { ?>
                        <?php $delay = 200 * $i; ?>
                        <div class="service-item col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
                            <div class="service-item-wrapper">
                                <div class="service-item-front-wrapper">
                                    <?php $bg_banner = wp_get_attachment_image_src($item['icon_id'], 'avatar', false); ?>
                                    <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                    <h3><?php echo apply_filters('the_content', $item['title']); ?></h3>
                                    <div class="service-item-front-chevron">
                                        <i class="fa fa-chevron-up"></i>
                                        <span><?php _e('+ info', 'sellcrew'); ?></span>
                                    </div>
                                </div>
                                <div class="service-item-back-wrapper">
                                    <?php echo $item['desc']; ?>
                                </div>
                            </div>
                        </div>
                    <?php $i++;
                        $i = ($i > 3) ? 1 : $i;
                    } ?>
                </div>
            </div>
        </section>

        <section class="howto-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="howto-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'scw_home_howto_title', true); ?></h2>
                    </div>
                    <div class="howto-steps-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row row-steps">
                                <?php $services_list = get_post_meta(get_the_ID(), 'scw_home_howto_group', true); ?>
                                <?php $i = 1; ?>
                                <?php foreach ($services_list as $item) { ?>
                                    <?php $delay = 200 * $i; ?>
                                    <div class="step-item col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12" data-aos="fade-in" data-aos-delay="<?php echo $delay; ?>">
                                        <div class="step-item-wrapper">
                                            <h3><?php _e('Paso', 'sellcrew'); ?> <span><?php echo $i; ?></span></h3>
                                            <?php echo apply_filters('the_content', $item['title']); ?>
                                        </div>
                                    </div>
                                <?php $i++;
                                } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="plans-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="plans-background">
                <img src="<?php echo get_template_directory_uri(); ?>/images/plans-vector.png" alt="bubble1" class="parallax-handler">
            </div>
            <div class="container">
                <div class="row">
                    <div class="plans-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'scw_home_plans_title', true); ?></h2>
                    </div>
                </div>
                <div class="row row-eq-height">
                    <?php $services_list = get_post_meta(get_the_ID(), 'scw_home_plans_group', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($services_list as $item) { ?>
                        <?php if ($i == 1) {
                            $class = 'left';
                        } ?>
                        <?php if ($i == 2) {
                            $class = 'in';
                        } ?>
                        <?php if ($i == 3) {
                            $class = 'right';
                        } ?>
                        <div id="plan<?php echo $i; ?>" class="plans-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" data-aos="fade-<?php echo $class; ?>" data-aos-delay="300">
                            <div class="plans-item-wrapper">
                                <?php $bg_banner = wp_get_attachment_image_src($item['icon_id'], 'avatar', false); ?>
                                <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                <h3><?php echo $item['title']; ?></h3>
                                <div class="plans-items-content">
                                    <?php echo apply_filters('the_content', $item['desc']); ?>
                                </div>
                                <div id="planModalContent<?php echo $i; ?>" class="plans-items-modal-content d-none">
                                    <h2><?php echo $item['title']; ?></h2>
                                    <?php echo apply_filters('the_content', $item['modal_desc']); ?>
                                </div>
                                <div class="plans-items-button">
                                    <a href="" data-plan="<?php echo $i; ?>" data-title="<?php echo $item['title']; ?>" class="btn btn-md btn-plan btn-special">
                                        <span class="btn__ink"></span>
                                        <div class="btn__inner">
                                            <?php _e('Ver plan', 'sellcrew'); ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php $i++;
                    } ?>
                </div>
            </div>
        </section>

        <section class="companies-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="companies-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'scw_home_companies_title', true); ?></h2>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="companies-gallery swiper-container">
                    <div class="swiper-wrapper">
                        <?php $gallery_list = get_post_meta(get_the_ID(), 'scw_home_companies_list', true); ?>
                        <?php $i = 1; ?>
                        <?php foreach ($gallery_list as $item) { ?>
                            <?php $delay = 50 * $i; ?>
                            <div class="swiper-slide" data-aos="fade-in" data-aos-delay="<?php echo $delay; ?>">
                                <div class="partners-item">
                                    <div class="companies-item-wrapper">
                                        <?php $bg_banner = wp_get_attachment_image_src($item['logo_id'], 'full', false); ?>
                                        <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['logo_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['logo_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid img-color" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />

                                        <?php $bg_banner = wp_get_attachment_image_src($item['logo_gray_id'], 'full', false); ?>
                                        <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['logo_gray_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['logo_gray_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid img-gray" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                    </div>
                                </div>
                            </div>
                        <?php $i++;
                        } ?>
                    </div>
                </div>
            </div>
        </section>


        <section class="testimonials-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="testimonial-before">
                <img src="<?php echo get_template_directory_uri(); ?>/images/testimonials-vector2.png" alt="bubble1" class="parallax-handler3">
            </div>
            <div class="testimonial-after">
                <img src="<?php echo get_template_directory_uri(); ?>/images/testimonials-vector1.png" alt="bubble2" class="parallax-handler4">
            </div>
            <div class="container">
                <div class="row">
                    <div class="testimonials-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'scw_home_testimonials_title', true); ?></h2>
                    </div>

                </div>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="testimonials-slider-container col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12">
                        <div class="testimonial-swiper swiper-container">
                            <div class="swiper-wrapper">
                                <?php $services_list = get_post_meta(get_the_ID(), 'scw_home_testimonials_group', true); ?>
                                <?php foreach ($services_list as $item) { ?>
                                    <div class="swiper-slide">
                                        <div class="container">
                                            <div class="row align-items-center">
                                                <div class="testimonial-logo  col-xl-6 col-lg-5 col-md-5 col-sm-12 col-12">
                                                    <?php $bg_banner = wp_get_attachment_image_src($item['logo_id'], 'large', false); ?>
                                                    <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade-in" data-aos-delay="350" />
                                                </div>
                                                <div class="testimonial-item-content col-xl-6 col-lg-7 col-md-7 col-sm-12 col-12">
                                                    <div class="testimonial-item-title" data-aos="fade-in" data-aos-delay="350">
                                                        <?php echo apply_filters('the_content', $item['title']); ?>
                                                    </div>
                                                    <div class="testimonial-content" data-aos="fade-in" data-aos-delay="350">
                                                        <?php echo apply_filters('the_content', $item['desc']); ?>
                                                    </div>
                                                    <div class="testimonial-item-meta" data-aos="fade-in" data-aos-delay="350">
                                                        <h4><?php echo $item['author']; ?></h4>
                                                        <span><?php echo $item['jobtitle']; ?></span>
                                                    </div>
                                                    <div class="testimonial-item-stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="about-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'scw_home_about_title', true); ?></h2>
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'scw_home_about_desc', true)); ?>
                    </div>
                    <div class="about-image col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="250">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'scw_home_about_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img loading="lazy" itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </div>
                    <div class="about-second-content col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="350">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'scw_home_about_desc2', true)); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="big-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="big-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2 data-aos="fade-in" data-aos-delay="250"><?php echo get_post_meta(get_the_ID(), 'scw_home_bighero_title', true); ?></h2>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'scw_home_bighero_button_url', true); ?>" class="btn btn-md btn-hero btn-special" data-aos="fade-in" data-aos-delay="350" data-toggle="modal" data-target="#contactModal">
                            <span class="btn__ink"></span>
                            <div class="btn__inner">
                                <?php echo get_post_meta(get_the_ID(), 'scw_home_bighero_button_text', true); ?>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>