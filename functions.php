<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action('wp_enqueue_scripts', 'sellcrew_jquery_enqueue');
function sellcrew_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.1.0', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script( 'jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.3.1.js', array('jquery'), '3.3.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action( 'after_setup_theme', 'sellcrew_register_navwalker' );
function sellcrew_register_navwalker(){
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */
if ( class_exists( 'WooCommerce' ) ) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */
if ( defined( 'JETPACK__VERSION' ) ) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header - Principal', 'sellcrew' )
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'sellcrew_widgets_init' );

function sellcrew_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'sellcrew' ),
        'id' => 'main_sidebar',
        'description' => __( 'Estos widgets seran vistos en las entradas y páginas del sitio', 'sellcrew' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebars( 4, array(
        'name'          => __('Pie de Página %d', 'sellcrew'),
        'id'            => 'sidebar_footer',
        'description'   => __('Estos widgets seran vistos en el pie de página del sitio', 'sellcrew'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

    //    register_sidebar( array(
    //        'name' => __( 'Sidebar de la Tienda', 'sellcrew' ),
    //        'id' => 'shop_sidebar',
    //        'description' => __( 'Estos widgets seran vistos en Tienda y Categorias de Producto', 'sellcrew' ),
    //        'before_widget' => '<li id='%1$s' class='widget %2$s'>',
    //        'after_widget'  => '</li>',
    //        'before_title'  => '<h2 class='widgettitle'>',
    //        'after_title'   => '</h2>',
    //    ) );
}



/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists('add_image_size') ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('logo', 9999, 60, false);
    add_image_size('blog_img', 276, 217, true);
    add_image_size('single_img', 636, 297, true );
}

/* --------------------------------------------------------------
    ADD CUSTOM AJAX HANDLER
-------------------------------------------------------------- */
add_action('wp_ajax_nopriv_send_message', 'send_message_handler');
add_action('wp_ajax_send_message', 'send_message_handler');

function send_message_handler() {
    if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );
    }

    $nombre = $_POST['modal_nombre'];
    $apellido = $_POST['modal_apellido'];
    $correo = $_POST['modal_correo'];
    $telefono = $_POST['modal_telefono'];
    $empresa = $_POST['modal_empresa'];
    $website = $_POST['modal_website'];
    $message = $_POST['modal_message'];
    $origin = $_POST['modal_origin'];

    $logo = get_template_directory_uri() . '/images/logo.png';
    //    $grecaptcha = $_POST['g-recaptcha-response'];

    //    $google_options = get_option('hlp_google_settings');

    //    if ($grecaptcha) {
    //        $post_data = http_build_query(
    //            array(
    //                'secret' => $google_options['secretkey'],
    //                'response' => $grecaptcha,
    //                'remoteip' => $_SERVER['REMOTE_ADDR']
    //            ), '', '&');
    //
    //        $opts = array('http' =>
    //                      array(
    //                          'method'  => 'POST',
    //                          'header'  => 'Content-type: application/x-www-form-urlencoded',
    //                          'content' => $post_data
    //                      )
    //                     );
    //
    //        $context  = stream_context_create($opts);
    //        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    //        $captcha_response = json_decode($response);
    //    }
    //
    //    if($captcha_response->success == true) {
    ob_start();
    require_once get_theme_file_path( '/templates/contact-form.php' );
    $body = ob_get_clean();
    $body = str_replace( [
        '{nombre}',
        '{apellido}',
        '{correo}',
        '{telefono}',
        '{empresa}',
        '{website}',
        '{message}',
        '{logo}'
    ], [
        $nombre,
        $apellido,
        $correo,
        $telefono,
        $empresa,
        $website,
        $message,
        $logo
    ], $body );

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }



    $mail->isHTML( true );
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress( 'info@sell-crew.com' );
	$mail->addCC( 'fashionusllc@gmail.com' );
    $mail->addBCC( 'nefrain@gmail.com' );
    $mail->setFrom( "noreply@{$_SERVER['SERVER_NAME']}", esc_html( get_bloginfo( 'name' ) ) );
    $mail->Subject = esc_html__( 'Sell-Crew: Nuevo Mensaje', 'sellcrew' );

    if ($origin == 'modal') {
        $content = home_url('ty_contacto');
    }

    if ( ! $mail->send() ) {
        //        wp_send_json_error( esc_html__( 'Se ha producido un error, por favor intente de nuevo.', 'sellcrew' ), 400 );
        wp_send_json_success( $content, 200 );
    } else {

        wp_send_json_success( $content, 200 );
    }

    //    } else {
    //        wp_send_json_error( esc_html__( 'Your request2 could not be sent. Please try again.', 'holpack' ), 400 );
    //    }

    wp_die();
}


add_action('wp_ajax_nopriv_plan_send_message', 'plan_send_message_handler');
add_action('wp_ajax_plan_send_message', 'plan_send_message_handler');

function plan_send_message_handler() {
    if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );
    }

    $nombre = $_POST['plan_nombre'];
    $apellido = $_POST['plan_apellido'];
    $correo = $_POST['plan_correo'];
    $telefono = $_POST['plan_telefono'];
    $empresa = $_POST['plan_empresa'];
    $website = $_POST['plan_website'];
    $message = $_POST['plan_message'];
    $origin = $_POST['plan_origin'];

    $logo = get_template_directory_uri() . '/images/logo.png';
    //    $grecaptcha = $_POST['g-recaptcha-response'];

    //    $google_options = get_option('hlp_google_settings');

    //    if ($grecaptcha) {
    //        $post_data = http_build_query(
    //            array(
    //                'secret' => $google_options['secretkey'],
    //                'response' => $grecaptcha,
    //                'remoteip' => $_SERVER['REMOTE_ADDR']
    //            ), '', '&');
    //
    //        $opts = array('http' =>
    //                      array(
    //                          'method'  => 'POST',
    //                          'header'  => 'Content-type: application/x-www-form-urlencoded',
    //                          'content' => $post_data
    //                      )
    //                     );
    //
    //        $context  = stream_context_create($opts);
    //        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    //        $captcha_response = json_decode($response);
    //    }
    //
    //    if($captcha_response->success == true) {
    ob_start();
    require_once get_theme_file_path( '/templates/contact-form.php' );
    $body = ob_get_clean();
    $body = str_replace( [
        '{nombre}',
        '{apellido}',
        '{correo}',
        '{telefono}',
        '{empresa}',
        '{website}',
        '{message}',
        '{logo}'
    ], [
        $nombre,
        $apellido,
        $correo,
        $telefono,
        $empresa,
        $website,
        $message,
        $logo
    ], $body );

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }



    $mail->isHTML( true );
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress( 'info@sell-crew.com' );
	$mail->addCC( 'fashionusllc@gmail.com' );
    $mail->addBCC( 'nefrain@gmail.com' );
    $mail->setFrom( "noreply@{$_SERVER['SERVER_NAME']}", esc_html( get_bloginfo( 'name' ) ) );
    $mail->Subject = esc_html__( 'Sell-Crew: Planes - Nuevo Mensaje', 'sellcrew' );

    if ($origin == 'MARKET RESEARCH') {
        $content = home_url('ty_market');
    }

    if ($origin == 'START PRO') {
        $content = home_url('ty_start');
    }

    if ($origin == 'GROWTH PRO') {
        $content = home_url('ty_growth');
    }

    if ( ! $mail->send() ) {
        //        wp_send_json_error( esc_html__( 'Se ha producido un error, por favor intente de nuevo.', 'sellcrew' ), 400 );
        wp_send_json_success( $content, 200 );
    } else {

        wp_send_json_success( $content, 200 );
    }

    //    } else {
    //        wp_send_json_error( esc_html__( 'Your request2 could not be sent. Please try again.', 'holpack' ), 400 );
    //    }

    wp_die();
}