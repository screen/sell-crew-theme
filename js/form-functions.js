var submitBtn = document.getElementById('submitBtn'),
    submitPlanBtn = document.getElementById('submitPlanBtn'),
    passd = true;

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function telephoneCheck(str) {
    var patt = new RegExp(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm);
    return patt.test(str);
}

if (submitBtn != null) {
    submitBtn.addEventListener('click', function (e) {
        e.preventDefault();
        passd = true;

        /* NAME VALIDATION  */
        var inputText = document.getElementById('modal_nombre').value;
        var elements = document.getElementsByClassName('error-nombre');
        if (inputText == '' || inputText == null) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = custom_admin_url.error_nombre;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_nombre;
                passd = false;
            } else {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = '';
            }
        }

        /* APELLIDO VALIDATION  */
        var inputText = document.getElementById('modal_apellido').value;
        var elements = document.getElementsByClassName('error-apellido');
        if (inputText == '' || inputText == null) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = custom_admin_url.error_apellido;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_apellido;
                passd = false;
            } else {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = '';
            }
        }

        /* EMAIL VALIDATION  */
        var inputText = document.getElementById('modal_correo').value;
        var elements = document.getElementsByClassName('error-correo');
        if (inputText == '' || inputText == null) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = custom_admin_url.error_email;
            passd = false;
        } else {
            if (validateEmail(inputText) == false) {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_email;
                passd = false;
            } else {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = '';
            }
        }

        if (passd == true) {
            var info = 'action=send_message&modal_nombre=' +
                document.getElementById('modal_nombre').value + '&modal_apellido=' +
                document.getElementById('modal_apellido').value + '&modal_correo=' + document.getElementById('modal_correo').value + '&modal_telefono=' + document.getElementById('modal_telefono').value + '&modal_empresa=' + document.getElementById('modal_empresa').value + '&modal_website=' + document.getElementById('modal_website').value + '&modal_message=' + document.getElementById('modal_message').value + '&modal_origin=' + document.getElementById('modalSelected').value;
            var loaderForm = document.getElementById('loaderForm');
            loaderForm.classList.add("contact-form-response-hidden");
            /* SEND AJAX */
            newRequest = new XMLHttpRequest();
            newRequest.open('POST', custom_admin_url.ajax_url, true);
            newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            newRequest.onload = function () {
                var result = JSON.parse(newRequest.responseText);
                var resultCont = document.getElementById('formResult');
                loaderForm.classList.toggle("contact-form-response-hidden");
                if (result.success == true) {
                    window.location.replace(result.data);
                } else {
                    window.location.replace(result.data);
                    resultCont.classList.remove('contact-form-result-hidden');
                    resultCont.innerHTML = result.data;
                }
            };
            newRequest.send(info);
        }

    });
}

if (submitPlanBtn != null) {
    submitPlanBtn.addEventListener('click', function (e) {
        e.preventDefault();
        passd = true;

        /* NAME VALIDATION  */
        var inputText = document.getElementById('plan_nombre').value;
        var elements = document.getElementsByClassName('plan-error-nombre');
        if (inputText == '' || inputText == null) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = custom_admin_url.error_nombre;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_nombre;
                passd = false;
            } else {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = '';
            }
        }

        /* APELLIDO VALIDATION  */
        var inputText = document.getElementById('plan_apellido').value;
        var elements = document.getElementsByClassName('plan-error-apellido');
        if (inputText == '' || inputText == null) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = custom_admin_url.error_apellido;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_apellido;
                passd = false;
            } else {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = '';
            }
        }

        /* EMAIL VALIDATION  */
        var inputText = document.getElementById('plan_correo').value;
        var elements = document.getElementsByClassName('plan-error-correo');
        if (inputText == '' || inputText == null) {
            if (elements[0].classList.contains('d-none')) {
                elements[0].classList.toggle("d-none");
            }
            elements[0].innerHTML = custom_admin_url.error_email;
            passd = false;
        } else {
            if (validateEmail(inputText) == false) {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_email;
                passd = false;
            } else {
                if (elements[0].classList.contains('d-none')) {
                    elements[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = '';
            }
        }

        if (passd == true) {
            var info = 'action=plan_send_message&plan_nombre=' +
                document.getElementById('plan_nombre').value + '&plan_apellido=' +
                document.getElementById('plan_apellido').value + '&plan_correo=' + document.getElementById('plan_correo').value + '&plan_telefono=' + document.getElementById('plan_telefono').value + '&plan_empresa=' + document.getElementById('plan_empresa').value + '&plan_website=' + document.getElementById('plan_website').value + '&plan_message=' + document.getElementById('plan_message').value + '&plan_origin=' + document.getElementById('planSelected').value;
            var loaderPlan = document.getElementById('loaderPlan');
            loaderPlan.classList.remove("contact-form-response-hidden");
            /* SEND AJAX */
            newRequest = new XMLHttpRequest();
            newRequest.open('POST', custom_admin_url.ajax_url, true);
            newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            newRequest.onload = function () {
                var result = JSON.parse(newRequest.responseText);
                var resultPlan = document.getElementById('resultPlan');
                loaderPlan.classList.add("contact-form-response-hidden");
                if (result.success == true) {
                    window.location.replace(result.data);
                } else {
                    window.location.replace(result.data);
                    resultPlan.classList.remove('contact-form-result-hidden');
                    resultPlan.innerHTML = result.data;
                }
            };
            newRequest.send(info);
        }

    });
}




jQuery('#contactModal').on('hidden.bs.modal', function (e) {
    var loaderForm = document.getElementById('loaderPlan');
    loaderForm.classList.add("contact-form-response-hidden");

    var resultForm = document.getElementById('resultPlan');
    resultForm.classList.add('contact-form-result-hidden');

    document.getElementById("mainForm").reset();
});

jQuery('#planModal').on('hidden.bs.modal', function (e) {
    var loaderPlan = document.getElementById('loaderPlan');
    loaderPlan.classList.add("contact-form-response-hidden");

    var resultPlan = document.getElementById('resultPlan');
    resultPlan.classList.add('contact-form-result-hidden');
});
