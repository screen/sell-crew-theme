var maxHeight = 0,
    plansContent = '',
    serviceOpener = '',
    serviceCloser = '';

AOS.init({
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document,
    offset: 80, // offset (in px) from the original trigger point
    delay: 0, // values from 0 to 3000, with step 50ms
    duration: 400, // values from 0 to 3000, with step 50ms
    easing: 'ease-in-out', // default easing for AOS animations
    mirror: false,
    once: true
});

function documentCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    var last_known_scroll_position = 0;
    var ticking = false;

    function moveParallax(scroll_pos) {
        var parallaxElm1 = document.getElementsByClassName('parallax-layer__1');
        var parallaxElm2 = document.getElementsByClassName('parallax-layer__3');
        var qtyLeft = (scroll_pos) / 15;
        var opacity = scroll_pos / (scroll_pos - 100);
        var qtyRight = (scroll_pos) / 15;
        if (qtyLeft < 8) {
            parallaxElm1[0].style.removeProperty('left');
            parallaxElm1[0].style.opacity = 1;
        } else {
            parallaxElm1[0].style.left = '-' + qtyLeft + '%';
            parallaxElm1[0].style.opacity = opacity;
        }

        if (qtyRight < 8) {
            parallaxElm2[0].style.removeProperty('right');
            parallaxElm2[0].style.opacity = 1;
        } else {
            parallaxElm2[0].style.right = '-' + qtyRight + '%';
            parallaxElm2[0].style.opacity = opacity;
        }
    }

    window.addEventListener('scroll', function (e) {
        last_known_scroll_position = window.scrollY;
        if (!ticking) {
            window.requestAnimationFrame(function () {
                moveParallax(last_known_scroll_position);
                ticking = false;
            });
        }
        ticking = true;
    });

    var planOpenModal = document.getElementsByClassName('btn-plan');
    if (planOpenModal != null) {
        for (var i = 0; i < planOpenModal.length; i++) {

            planOpenModal[i].addEventListener("click", function (e) {
                e.preventDefault();
                var planTitle = this.dataset.title;
                var textCopy = document.getElementById('planModalContent' + this.dataset.plan);
                var textPaste = document.getElementById('planInfoModal');
                textPaste.innerHTML = '';
                textPaste.innerHTML = textCopy.innerHTML;
                document.getElementById('planSelected').value = planTitle;
                jQuery('#planModal').modal('show');
            }, false);
        }
    }

    var parallaxDivs = document.getElementsByClassName('parallax-handler');
    new simpleParallax(parallaxDivs, {
        scale: 2,
        delay: 0.1,
        overflow: true
    });

    var parallaxDivs2 = document.getElementsByClassName('parallax-handler2');
    new simpleParallax(parallaxDivs2, {
        scale: 2,
        delay: 0.1,
        overflow: true
    });

    var parallaxDivs3 = document.getElementsByClassName('parallax-handler3');
    new simpleParallax(parallaxDivs3, {
        scale: 2,
        delay: 0.1,
        overflow: true
    });

    var parallaxDivs4 = document.getElementsByClassName('parallax-handler4');
    new simpleParallax(parallaxDivs4, {
        scale: 2,
        delay: 0.1,
        overflow: true
    });

    var swiper = new Swiper('.testimonial-swiper', {
        slidesPerView: 1,
        loop: true,
        effect: 'fade',
        pagination: {
            el: '.swiper-pagination',
        },
        fadeEffect: {
            crossFade: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });



    var swiperLogos1 = new Swiper('.partners-gallery', {
        slidesPerView: 4,
        spaceBetween: 100,
        loop: true,

        autoplay: {
            delay: 4500,
            disableOnInteraction: false,
        },
        breakpoints: {
            // when window width is >= 320px
            0: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 60
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 3,
                spaceBetween: 80
            },
            991: {
                slidesPerView: 4,
                spaceBetween: 100
            }
        }
    });

    var swiperLogos2 = new Swiper('.companies-gallery', {
        slidesPerView: 8,
        spaceBetween: 100,
        loop: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        breakpoints: {
            // when window width is >= 320px
            0: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 6,
                spaceBetween: 40
            },
            1170: {
                slidesPerView: 6,
                spaceBetween: 40
            },
            1280: {
                slidesPerView: 8,
                spaceBetween: 40
            }
        }
    });

    var $btn = $('.btn'),
        $btnInk = $btn.find('.btn__ink');
    if (!$btnInk.height() && !$btnInk.width()) {
        var d = Math.max($btn.outerWidth(), $btn.outerHeight());
        $btnInk.css({
            height: d,
            width: d
        });
    }
    $btn.on('mouseenter', function (e) {
        var $rippler = $(this),
            $ink = $(this)
            .find('.btn__ink');
        $ink.removeClass('btn__ink--active');
        var x = e.pageX - $rippler.offset()
            .left - $ink.width() / 3;
        var y = e.pageY - $rippler.offset()
            .top - $ink.height() / 3;
        $ink.css({
                top: y + 'px',
                left: x + 'px'
            })
            .addClass('btn__ink--active');
    });
    $btn.on('mouseleave', function (e) {
        var $rippler = $(this),
            $ink = $(this)
            .find('.btn__ink');
        var x = e.pageX - $rippler.offset()
            .left - $ink.width() / 3;
        var y = e.pageY - $rippler.offset()
            .top - $ink.height() / 3;
        $ink.css({
                top: y + 'px',
                left: x + 'px'
            })
            .removeClass('btn__ink--active');
    });

    plansContent = document.getElementsByClassName('plans-items-content');
    if (plansContent != null) {
        maxHeight = 0;
        for (var i = 0; i < plansContent.length; i++) {
            if (plansContent[i].offsetHeight > maxHeight) {
                maxHeight = plansContent[i].offsetHeight;
            }
        }
        for (var i = 0; i < plansContent.length; i++) {
            plansContent[i].style.height = maxHeight + 'px';
        }
    }
    window.addEventListener("resize", recalculateWindow, false);
}

function recalculateWindow() {
    plansContent = document.getElementsByClassName('plans-items-content');
    if (window.screen.width > 767) {
        if (plansContent != null) {
            maxHeight = 0;
            for (var i = 0; i < plansContent.length; i++) {
                plansContent[i].style.height = 'auto';
                if (plansContent[i].offsetHeight > maxHeight) {
                    maxHeight = plansContent[i].offsetHeight;
                }
            }
            for (var i = 0; i < plansContent.length; i++) {
                plansContent[i].style.height = maxHeight + 'px';
            }
        }
    } else {
        if (plansContent != null) {
            for (var i = 0; i < plansContent.length; i++) {
                plansContent[i].style.height = 'auto';
            }
        }
    }
}
document.addEventListener("DOMContentLoaded", documentCustomLoad, false);

function Parallax(options) {
    options = options || {};
    this.nameSpaces = {
        wrapper: options.wrapper || '.parallax',
        layers: options.layers || '.parallax-layer',
        deep: options.deep || 'data-parallax-deep'
    };
    this.init = function () {
        var self = this,
            parallaxWrappers = document.querySelectorAll(this.nameSpaces.wrapper);
        for (var i = 0; i < parallaxWrappers.length; i++) {
            (function (i) {
                document.body.addEventListener('mousemove', function (e) {
                    var x = e.clientX,
                        y = e.clientY,
                        layers = parallaxWrappers[i].querySelectorAll(self.nameSpaces.layers);
                    for (var j = 0; j < layers.length; j++) {
                        (function (j) {
                            var deep = layers[j].getAttribute(self.nameSpaces.deep),
                                disallow = layers[j].getAttribute('data-parallax-disallow'),
                                itemX = (disallow && disallow === 'x') ? 0 : x / deep,
                                itemY = (disallow && disallow === 'y') ? 0 : y / deep;
                            if (disallow && disallow === 'both') return;
                            layers[j].style.transform = 'translateX(' + itemX + '%) translateY(' + itemY + '%)';
                        })(j);
                    }
                })
            })(i);
        }
    };
    this.init();
    return this;
}
window.addEventListener('load', function () {
    new Parallax();
});

function mouseMove(e) {
    let target = e.target.closest("a"),
        targetData = target.getBoundingClientRect(),
        offset = {
            x: ((e.pageX - (targetData.left + targetData.width / 2)) / 4) * -1,
            y: ((e.pageY - (targetData.top + targetData.height / 2)) / 4) * -1
        };
    target.style.transform = "translate(" + offset.x + "px," + offset.y + "px) scale(" + 1.1 + ")";
    target.style.webkitTransform = "translate(" + offset.x + "px," + offset.y + "px) scale(" + 1.1 + ")";
}
